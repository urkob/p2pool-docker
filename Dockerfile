from ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Madrid
RUN apt-get update
RUN apt-get install -y git build-essential cmake libuv1-dev libzmq3-dev libsodium-dev libpgm-dev libnorm-dev libgss-dev
RUN git clone --recursive https://github.com/SChernykh/p2pool
RUN ls -la
RUN cd p2pool && mkdir build
WORKDIR "/p2pool/build"
RUN ls -la
RUN cmake ..
RUN make -j$(nproc)
